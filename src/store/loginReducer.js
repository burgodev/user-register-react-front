const ACTIONS = {
  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
};

const INITIAL_STATE = {
  loggedUser: {},
  isAdm: false,
  isLogged: false,
};

export const loginReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case ACTIONS.LOGIN:
      return {
        ...state,
        loggedUser: action.user,
        isLogged: true,
        isAdm: action.user.isAdm,
      };

    case ACTIONS.LOGOUT :
      return {...state, isLogged: false};

    default :
      return state;
  }
};

export function login(user) {
  return dispatch => {
    dispatch({
      type: ACTIONS.LOGIN,
      user: user
    })
  }
}

export function logout() {
  return {
    type: ACTIONS.LOGOUT
  };
}


