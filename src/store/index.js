import { combineReducers } from 'redux';
import { usersReducer } from './usersReducer';
import { dialogReducer } from './dialogReducer';
import { loginReducer } from './loginReducer';



const mainReducer = combineReducers({
  users: usersReducer,
  dialog: dialogReducer,
  loginReducer: loginReducer
});

export default mainReducer;

