const ACTIONS = {
  OPEN_DIALOG: 'OPEN_DIALOG',
  CLOSE_DIALOG: 'CLOSE_DIALOG'
};

const INITIAL_STATE = {
  open: false,
  user: '',
  edit: false,
};

export const dialogReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACTIONS.OPEN_DIALOG :
      return { ...state, user: action.user, open: true, edit: action.edit};
    case ACTIONS.CLOSE_DIALOG :
      return { ...state, text: '', open: false };
    default :
      return state;
  }
};

export function openDialog(user, edit) {
  return {
    type: ACTIONS.OPEN_DIALOG,
    edit: edit,
    user: user,
  };
}

export function closeDialog() {
  return {
    type: ACTIONS.CLOSE_DIALOG
  };
}
