import axios from 'axios'

const http = axios.create({
  baseURL: 'http://localhost:3000/'
});

const ACTIONS = {
  GET: 'GET_USERS',
  POST: 'POST_USERS',
  CHANGE_STATUS: 'CHANGE_STATUS',
  UPDATE: 'UPDATE_USERS',
};

const INITIAL_STATE = {
  users: [],
};

export const usersReducer = (state = INITIAL_STATE, action) => {
  let usersList;

  switch (action.type) {
    case ACTIONS.GET :
      return {
        ...state,
        users: action.users,
      };

    case ACTIONS.POST :
      usersList = [...state.users, action.users];

      return {
        ...state,
        users: usersList,
      };

    case ACTIONS.CHANGE_STATUS :
      return {
        ...state,
        users: action.users
      };

    case ACTIONS.UPDATE:
      return {
        ...state,
        users: action.users,
      };

    default:
      return state;
  }
};


export function getUsers(name) {
  if (typeof name === 'undefined'  || name === '') {
    return dispatch => {
      console.log('getUsers all');
      http.get('/users').then(response => {
        dispatch({
          type: ACTIONS.GET,
          users: response.data
        })
      })
    }
  } else {
    return dispatch => {
      console.log('get user by name');
      http.get(`/users/?name=${name}`).then(response => {
        console.log('user by name response', response.data)
        dispatch({
          type: ACTIONS.GET,
          users: response.data
        })
      })
    }
  }
}

export function getUserByName(name) {
  return dispatch => {
    dispatch({
      type: ACTIONS.GET,
      users: {
        name: 'teste'
      }
    })
  }
}

export function postUser(user) {
  return dispatch => {
    http.post(`/users/`, user, {
      headers: {'Content-Type': 'application/json'}
    }).then(response => {
      dispatch({
        type: ACTIONS.POST,
        users: response.data
      })
    });
  }
}

export function changeStatus(user) {
  return dispatch => {
    if (user.isActive) {
      user.isActive = false;
    } else {
      user.isActive = true;
    }

    http.put(`/users/${user.id}`, user, {
      headers: {'Content-Type': 'application/json'}
    }).then(response => {
      http.get('/users').then(response => {
        dispatch({
          type: ACTIONS.CHANGE_STATUS,
          users: response.data
        })
      })
    });
  }
}

export function updateUser(user) {
  return dispatch => {
    http.put(`/users/${user.id}`, user, {
      headers: {'Content-Type': 'application/json'}
    }).then(response => {
      http.get('/users').then(response => {
        console.log('respojnse', response.data)
        dispatch({
          type: ACTIONS.UPDATE,
          users: response.data
        })
      })
    });
  }
}



