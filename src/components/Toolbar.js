import React from 'react';
import {fade, makeStyles} from '@material-ui/core/styles';
//COMPONENTS
import {
  Button,
  AppBar,
  Toolbar,
  Typography,
  Box,
  TextField,
  Icon,
}
  from '@material-ui/core';
import FormDialog from '../components/Dialog';
//REDUX
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {openDialog} from '../store/dialogReducer';
import {getUsers} from '../store/usersReducer';

//STYLES
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    textAlign: 'start',
    flexGrow: 1,
    color: '#000000',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },

  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },

  textStart: {
    textAlign: 'start',
  },

  textColor: {
    color: '#ffffff'
  },

  MuiAppBarColorPrimary: {
    color: '#ffffff',
    backgroundColor: '#F5F5F5'
  },

  alignCenter: {
    alignItems: 'center',
  },

  selfEnd: {
    alignSelf: 'flex-end'
  },

  tab: {
    boxShadow: '1 !important',
    padding: '0',

    textAlign: 'center',
    borderRadius: '0 !important',
  },

  usersTitle: {
    textAlign: 'start',
    flexGrow: 1,
    paddingLeft: '24px',
    paddingBottom: '7px',
    color: '#000000',
    borderBottom: 'solid orange',
    maxWidth: '150px',
    fontWeight: '400'
  }
}));

const SearchAppBar = (props => {
  const [state, setState] = React.useState({
    name: '',
  });

  const classes = useStyles();

  const handleChange = event => {
    setState({...state, 'name': event.target.value});
  };

  const search = () => {
    props.getUsers(state.name);
    setState({...state, 'name': ''});
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.MuiAppBarColorPrimary}>
        <Toolbar>
          <div style={{width: '100%'}}>
            <Box display="flex" p={1} className={classes.alignCenter}>

              <Box p={1} flexGrow={1} className={classes.textStart}>
                <Typography className={classes.title} variant="h4" noWrap>
                  Gerenciar usuários
                </Typography>
              </Box>

              <Box p={1} flexGrow={0}>
                <Icon color="primary" className={'mb-4'}>search</Icon>
              </Box>
              <Box p={1} flexGrow={.2} fontStyle="italic">
                <TextField
                  fullWidth
                  id="input-with-icon-grid"
                  label="Buscar por usuário"
                  onChange={handleChange}
                  value={state.name}
                />
                <Button className={'mt-1'} onClick={e => search()}> Procurar </Button>
              </Box>

              <Box p={1} flexGrow={0}>
                <Icon color="primary" style={{fontSize: 40}}>person_pin</Icon>
              </Box>
              <Box p={1} flexGrow={0}>
                <Typography className={classes.title} variant="caption" noWrap>
                  {props.loggedUser.name}<br/>

                </Typography>
                <Typography className={classes.title} variant="caption" noWrap>
                  {props.loggedUser.isAdm ? 'Admin' : 'Usuário comum'}
                </Typography>
              </Box>
            </Box>
          </div>
        </Toolbar>
      </AppBar>

      <div className="my-container">
        <div className="row align-items-end row-register">
          <div className="col-10">
            <Typography className={classes.usersTitle} variant="h6" noWrap>
              Usuários
            </Typography>
          </div>
          <div className="col-2 mb-3">
            {
              props.loggedUser.isAdm ?
                <Button variant="contained" color="primary" onClick={() => props.openDialog('', false)}>
                  Cadastrar
                </Button>
                :
                null
            }
          </div>
        </div>
      </div>
      <FormDialog/>
    </div>
  );
});


const mapStateToProps = state => ({
  loggedUser: state.loginReducer.loggedUser
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
      openDialog,
      getUsers
    },
    dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SearchAppBar);
