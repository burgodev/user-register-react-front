import React from 'react';
//COMPONENTS
import {
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Switch,
  TextField
} from '@material-ui/core';
//REDUX
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {closeDialog, openDialog} from '../store/dialogReducer';
import {postUser, updateUser} from '../store/usersReducer';
//ROUTER
import PropTypes from "prop-types";
import {withRouter} from "react-router";



const FormDialog = props => {
  const [state, setState] = React.useState({
    isActive: true,
    adm: true,
    standard: false,
    name: '',
    surname: '',
    email: '',
    password: '',
    id: null,
  });


  const handleChange = name => event => {
    if (name === 'adm' && state.standard === true) {
      setState({...state, 'adm': event.target.checked, 'standard': false});
    } else if (name === 'standard' && state.adm === true) {
      setState({...state, 'standard': event.target.checked, 'adm': false});
    } else if (name === 'isActive') {
      setState({...state, [name]: event.target.checked});
    }

    switch (name) {
      case 'name' :
        setState({...state, 'name': event.target.value});
        break;
      case 'surname' :
        setState({...state, 'surname': event.target.value});
        break;
      case 'email' :
        setState({...state, 'email': event.target.value});
        break;
      case 'password' :
        setState({...state, 'password': event.target.value});
        break;
      default:
        break;
    }
  };


  const handleSubmit = () => {
    let isAdm, id;

    if (props.user.id) {
      id = props.user.id
    } else {
      props.users.forEach(user => {
        id = user.id + 1;
      });
    }

    state.adm ? isAdm = true : isAdm = false;

    let user = {
      id: id,
      name: state.name,
      surname: state.surname,
      email: state.email,
      password: state.password,
      isActive: state.isActive,
      isAdm: isAdm,
    };

    props.edit ? props.updateUser(user) : props.postUser(user);

    setState({...state,
      'adm': false,
      'standard': true,
      'name': '',
      'surname': '',
      'email': '',
      'password': '',
    });

    props.closeDialog();
  };

  return (
    <Dialog open={props.open} onClose={props.closeDialog} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">{props.edit ? 'Editar usuário' : 'Cadastrar usuário'}</DialogTitle>
      <DialogContent>
        <FormControlLabel
          control={
            <Switch
              checked={state.isActive}
              onChange={handleChange('isActive')}
              value="standard"
              color="primary"
            />
          }
          label="Usuário Ativo"
        />
        <br/>
        <span> Tipo de usuário</span>
        <br/>
        <FormControlLabel
          control={
            <Checkbox
              checked={state.adm}
              onChange={handleChange('adm')}
              color="primary"
            />
          }
          label="Administrador"
        />
        <br/>
        <FormControlLabel
          control={
            <Checkbox
              checked={state.standard}
              onChange={handleChange('standard')}
              color="primary"
            />
          }
          label="Usuário padrão"
        />
        <br/>
        <TextField
          autoFocus
          value={state.name}
          margin="dense"
          label="Nome*"
          type="text"
          fullWidth
          onChange={handleChange('name')}
        />
        <TextField
          margin="dense"
          value={state.surname}
          label="Sobrenome*"
          type="text"
          fullWidth
          onChange={handleChange('surname')}
        />
        <TextField
          margin="dense"
          value={state.email}
          label="E-mail*"
          type="email"
          fullWidth
          onChange={handleChange('email')}
        />
        <TextField
          margin="dense"
          value={state.password}
          label="Senha*"
          type="password"
          fullWidth
          onChange={handleChange('password')}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={props.closeDialog}>
          Cancelar
        </Button>
        <Button onClick={handleSubmit} color="primary">
          {props.edit ? 'Editar' : 'Cadastrar'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

FormDialog.propTypes = {
  history: PropTypes.object
};


const mapStateToProps = state => ({
  open: state.dialog.open,
  user: state.dialog.user,
  edit: state.dialog.edit,
  users: state.users.users,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
      closeDialog,
      openDialog,
      postUser,
      updateUser
    },
    dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(FormDialog));





