import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
//COMPONENTS
import {
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  Paper,
  TableRow
} from '@material-ui/core';
import FormDialog from '../components/Dialog';
//ICONS
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import CheckIcon from '@material-ui/icons/Check';
//REDUX
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {openDialog} from '../store/dialogReducer';
import {changeStatus, getUsers} from '../store/usersReducer';
//ROUTER
import {withRouter} from "react-router";
import PropTypes from 'prop-types';


let showActions;
//TABLE TITLES
const headCells = [
  {id: 'users', numeric: false, disablePadding: true, label: 'Usuários'},
  {id: 'isAdm', numeric: false, disablePadding: false, label: 'Tipo de usuario'},
  {id: 'isActive', numeric: false, disablePadding: false, label: 'Usuario ativo'},
];

//TABLE HEADER
const UsersTableHead = () => {
  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox"/>
        {
          headCells.map(headCell => (
            <TableCell
              key={headCell.id}
              align={headCell.numeric ? 'right' : 'left'}
              padding={headCell.disablePadding ? 'none' : 'default'}

            >
              {headCell.label}
            </TableCell>
          ))
        }
        {
          showActions ?
            <TableCell
              key={'actions'}
              align={'left'}
              padding={'default'}
            >
              Ações
            </TableCell>
            : <TableCell/>
        }
      </TableRow>
    </TableHead>
  );
};

UsersTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  rowCount: PropTypes.number.isRequired,
};

//STYLES
const useStyles = makeStyles(theme => ({
  root: {
    width: '90%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  nameCol: {
    width: '40vw',
  },
  actionCol: {
    paddingLeft: '0'
  }
}));


const UsersTable = props => {
  showActions = props.isAdm;

  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const {history} = props;

  const handleChangeStatus = (user) => {
    props.changeStatus(user);
    history.push('/usuarios');
  };

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, props.users.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={'medium'}
            aria-label="enhanced table"
          >
            <UsersTableHead
              classes={classes}
              rowCount={props.users.length}
            />
            <TableBody>
              {props.users.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((user, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      tabIndex={-1}
                      key={user.name}
                    >
                      <TableCell/>
                      <TableCell className={classes.nameCol} component="th" id={labelId} scope="row" padding="none">
                        {user.name}
                      </TableCell>

                      <TableCell>
                        {user.isAdm ? 'Administrador' : 'Usuário comum'}
                      </TableCell>

                      <TableCell>
                        {user.isActive ? 'Sim' : 'Não'}
                      </TableCell>


                      {props.isAdm ?
                        <TableCell className={classes.actionCol}>
                          <IconButton onClick={e => props.openDialog(user, true)}>
                            <EditIcon/>
                          </IconButton>
                          <IconButton onClick={e =>
                            handleChangeStatus(user)
                          }
                          >
                            {
                              user.isActive ? <DeleteIcon/> : <CheckIcon/>
                            }

                          </IconButton>
                        </TableCell>
                        :
                        <TableCell/>
                      }
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{height:  (53) * emptyRows}}>
                  <TableCell colSpan={6}/>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={props.users.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      <FormDialog/>
    </div>
  );
};

UsersTable.propTypes = {
  history: PropTypes.object
};


const mapStateToProps = state => ({
  isAdm: state.loginReducer.isAdm,
  users: state.users.users
});


const mapDispatchToProps = dispatch =>
  bindActionCreators({
      openDialog,
      changeStatus,
      getUsers
    },
    dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(UsersTable));


