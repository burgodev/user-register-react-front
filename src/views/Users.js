import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
//COMPONENTS
import Grid from '@material-ui/core/Grid';
import UsersTable from '../components/Table';
import SearchAppBar from '../components/Toolbar'
//REDUX
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {getUsers, postUser, updateUser} from '../store/usersReducer';
//ROUTER
import PropTypes from 'prop-types';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },

  header: {
    background: 'red',
    padding: '0',
    height: '10vh',
    textAlign: 'center',
    borderRadius: '0 !important',
  },

  gridItem: {
    padding: '0'
  },

  tab: {
    boxShadow: '1 !important',
    padding: '0',
    height: '10vh',
    textAlign: 'center',
    borderRadius: '0 !important',
  },

  table: {
    background: 'green',
    padding: '0',
    height: '10vh',
    textAlign: 'center',
    borderRadius: '0 !important',
  },

  center: {
    textAlign: '-webkit-center',
  }
}));

const Users = (props => {
  const classes = useStyles();
  const {history} = props;

  useEffect(() => {
    props.getUsers();

    if(!props.isLogged){
      history.push('/');
    }
  }, []);


    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <SearchAppBar/>
          </Grid>

          <Grid item xs={12} sm={12} lg={12} md={12} className={classes.center}>
            <UsersTable users={props.users} get={props.get} loggedUser={props.isAdm}/>
          </Grid>

        </Grid>
      </div>
    );
});

Users.propTypes = {
  history: PropTypes.object
};


const mapStateToProps = state => ({
  users: state.users.users,
  isAdm: state.loginReducer.isAdm,
  isLogged: state.loginReducer.isLogged
});


const mapDispatchToProps = dispatch =>
  bindActionCreators({
      getUsers,
      postUser,
      updateUser,
    },
    dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Users);

