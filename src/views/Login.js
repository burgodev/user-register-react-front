import React from 'react';
//COMPONENTS
import {Button, Card, CardActions, CardContent, Grid, IconButton, makeStyles, TextField} from '@material-ui/core';
//ICONS
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
//ROUTER
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
//REDUX/STORE
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {login} from '../store/loginReducer';

//axios
import axios from 'axios';
const http = axios.create({
  baseURL : 'http://localhost:3000/'
});

const useStyles = makeStyles({
    root: {
      width: '400px',
      height: '250px',
      textAlign: '-webkit-center',
    },

    cardActionLogin: {
      justifyContent: 'center',
      alignItems: 'center',
    }
  })
;

const Login = props => {
  const [state, setState] = React.useState({
    email: 'usuario1@email.com.br',
    password: '123445',
    showPassword: false,
  });

  const {history} = props;
  const classes = useStyles();

  const handleLogin = event => {
    event.preventDefault();

    http.get(`/users/?email=${state.email}`).then( response => {
      if(response.data.length > 0){
        if(response.data[0].password === state.password){
          props.login(response.data[0], response.data[0], response.data[0]);
          history.push('/usuarios');
        }else{
          alert('Senha incorreta... Tente novamente! :)')
        }
      } else{
        alert('Email incorreto... Tente novamente! :)')
      }
    })
  };

  const handleChange = name => event => {
    if (name === 'email') {
      setState({...state, 'email': event.target.value});
    } else {
      setState({...state, 'password': event.target.value});
    }
  };

  const handleClickShowPassword = () => {
    setState({...state, showPassword: !state.showPassword});
  };

  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="center"
      className={'height-100'}
    >
      <Card className={classes.root}>

        <Grid item xs={10}>
          <CardContent>
            <TextField
              autoFocus
              onChange={handleChange('email')}

              value={state.email}
              margin="dense"
              label="Email*"
              type="email"
              fullWidth
            />

            <TextField
              onChange={handleChange('password')}

              value={state.password}
              margin="dense"
              label="Senha"
              type={state.showPassword ? 'text' : 'password'}
              fullWidth
            />

            <IconButton
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
            >
              {state.showPassword ? <Visibility/> : <VisibilityOff/>}
            </IconButton>
          </CardContent>
        </Grid>
        <Grid item xs={10}>
          <CardActions className={classes.cardActionLogin}>
            <Button onClick={handleLogin} variant="contained" size={'large'} color="primary">
              Entrar
            </Button>
          </CardActions>
        </Grid>
      </Card>
    </Grid>
  );
}


Login.propTypes = {
  history: PropTypes.object
};

const mapStateToProps = state => ({
  users: state.users.users,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
      login
    },
    dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Login));
