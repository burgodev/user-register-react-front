import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Users from './views/Users'
import Login from './views/Login'

export default () => {
  return (

    <Switch>
      <Route exact path="/usuarios" component={Users} />
      <Route exact path="/" component={Login} />
    </Switch>

  )
}
