import React from 'react';
import './App.css';
import Routes from './routes'
import { HashRouter } from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <HashRouter>
        <div>
          <Routes/>
        </div>
      </HashRouter>
    </div>
  );
}

export default App;
